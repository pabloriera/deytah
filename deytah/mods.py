import dpath.util
import yaml
import re
from functools import reduce
import operator
import copy
from IPython import embed

def parse_path_to_dpath(config,path):
    #To do: ampliar para que pueda devolver multiples matches (no elegir correct_path[0])
    access_by_kv = re.findall('\[.*?\]',path)
    for special_token in access_by_kv:
        key = special_token.split('=')[0].replace("[","")
        val = special_token.split('=')[1].replace("]","")
        path_pre = path.split(special_token)[0]
        possible_paths = list(dpath.util.search(config,'{}*/{}'.format(path_pre,key),yielded = True))
        correct_path = [path[0] for path in possible_paths if str(path[1]) == val][0]
        parent_correct = '/'.join(correct_path.split('/')[:-1])
        path = parent_correct + path.split(special_token)[1]
    return path

def get_path(config,path):
    dpath_path = parse_path_to_dpath(config,path)
    dpaths_list = dpath.util.search(config,dpath_path,yielded=True)
    results = [dpath.util.get(config,dpath_i[0]) for dpath_i in dpaths_list]

    return results

def set_path(config,path,value):
    dpath_path = parse_path_to_dpath(config,path)
    dpaths_list = list(dpath.util.search(config,dpath_path,yielded=True))
    if len(dpaths_list)>0:
        for dpath_i in dpaths_list:
            dpath.util.set(config,dpath_i[0],value)
    else:
        add_path(config,path,value)

def add_path(config,path,value):
    dpath_path = parse_path_to_dpath(config,path)
    new_key = dpath_path.split('/')[-1]
    root = '/'.join(dpath_path.split('/')[:-1])
    notfound=1
    while notfound:
        dpaths_list = list(dpath.util.search(config,root,yielded=True))
        if len(dpaths_list)>0:
            notfound=0
            root_parent = '/'.join(root.split('/')[:-1])
            parent = get_path(config,root_parent)
            if len(parent)>0 and isinstance(parent[0],list):
                n_entries = len(parent[0])
                path = root_parent+'/{}/{}'.format(n_entries,new_key)
            else:
                root_path = list(dpath.util.search(config,root,yielded=True))[0][0]
                path = root_path+'/{}'.format(new_key)
        else:
            new_key = root.split('/')[-1] + '/' + new_key
            root = '/'.join(root.split('/')[:-1])
    print(path)
    dpath.util.new(config,path,value)
        
    
def nested_delete(root,items):
    items = [int(item) if item.isdigit() else item for item in items]
    parent = reduce(operator.getitem, items[:-1], root)
    operator.delitem(parent,items[-1])

def delete_path(config,path):
    dpath_path = parse_path_to_dpath(config,path)
    notempty = 1
    while notempty:
        dpaths_list = list(dpath.util.search(config,dpath_path,yielded=True))
        if len(dpaths_list)>0:
            dpath_i = dpaths_list[0]
            path_parts = dpath_i[0].split('/')
            nested_delete(config,path_parts)
        else: 
            notempty = 0

def apply_mods(config,mod):
    new_config = copy.deepcopy(config)
    if mod.startswith('delete:'):
        path = mod.split('delete:')[1]
        delete_path(new_config,path)
    else:
        mod_parts = mod.split('=')
        path = '='.join(mod_parts[:-1])
        value = yaml.load(mod_parts[-1], Loader=yaml.Loader)
        set_path(new_config,path,value)

    return new_config
