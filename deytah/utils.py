from pathlib import Path
import sys
import importlib

def add_dot(processor_name, string):
    if '.' not in string:
        return f'{processor_name}.{string}'
    else:
        return string


def check_dot(string):
    if '.' not in string:
        raise Exception('In and out addresses should use the syntax PROCESS.address')
    else:
        return string

def get_modules(module_paths):
    modules = {}
    for module_path in module_paths:
        module_path_ = str(Path(module_path).parent)
        sys.path.insert(0, module_path_)
        module_name = Path(module_path).stem
        modules[module_path] = importlib.import_module(module_name)
        sys.path.remove(module_path_)

    return modules
