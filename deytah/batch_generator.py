import pandas as pd
import numpy as np
from tensorflow.keras.utils import Sequence
from .core import DataProcessor
from IPython import embed
from copy import deepcopy
# gen_logger = logger.get_logger('BatchGenerator')
# gen_logger.setLevel(logging.INFO)


class BatchGenerator(Sequence):

    def set_data(self, x=None, y=None, data=None):
        self.data = data

        if x is not None and y is not None and self.data is None:
            self.data = pd.DataFrame({'x': x, 'y': y, 'index': np.arange(
                0, np.size(self.x, 0), dtype=np.int32)}).set_index('index', drop=True)
            self.x = x
            self.y = y
        elif self.data is None and self.x is None and self.y is None:
            raise Exception('Missing inputs (x,y or data)')
        # elif self.data is None:
            # raise Exception('X and Y needed')

        # if isinstance(self.data, pd.DataFrame):
        #     data_ = {name: self.data[name] for name in self.data.columns}
        #     data_['index'] = self.data.index
        #     self.data = data_

        # if 'index' not in self.data:
        #     raise Exception('"index" field must be present in data')

        if self.order_column is not None:
            if self.order_style == 'ascending':
                self.index = list(self.data.sort_values(
                    by=self.order_column, ascending=True).index)
            elif self.order_style == 'descending':
                self.index = list(self.data.sort_values(
                    by=self.order_column, ascending=False).index)
            elif self.order_style == 'interleaved':
                ordered_data = list(self.data.sort_values(
                    by=self.order_column, ascending=True).index)
                ascending_part = ordered_data[:len(ordered_data)//2]
                descending_part = ordered_data[:len(ordered_data)//2-1:-1]
                self.index = ascending_part + descending_part
                self.index[::2] = ascending_part
                self.index[1::2] = descending_part
        else:
            self.index = list(self.data.index)

        self.index_original = list(self.data.index)
        self.keys = list(self.data.keys())

    def __init__(self, x=None, y=None, data=None, batch_size=32, data_processor_config=None, shuffle=True,
                 input_names=None, output_names=None, in_memory=False, stratified=False, stratified_oversample=False, default_processors=True,
                 order_column=None, order_style='descending'):
        """
        x and y standard numpy arrays ala. sklearn can be specified
        to get data from, or they can be lists of filenames.
        Alternatively a dataframe or dictionary can be passed in 'data' argument.
        This way, multiple inputs and outputs can be given and a column name is assigned as identifier.
        """

        self.data_processor_config = deepcopy(data_processor_config)
        self.do_shuffle = shuffle
        self.in_memory = in_memory
        self.stratified_oversample = stratified_oversample
        self.stratified = stratified
        self.order_column = order_column
        self.order_style = order_style

        self.set_data(x=x, y=y, data=data)

        if input_names is None:
            # if 'x' in self.keys:
            #   self.input_names = ['x']
            # else:
            self.input_names = []
            # raise Exception('Input key names should be passed')
        else:
            if not isinstance(input_names, (list, tuple)):
                self.input_names = [input_names]
            else:
                self.input_names = input_names

        if output_names is None:
            # if 'y' in self.keys:
            #    self.output_names = ['y']
            # else:
            self.output_names = []
            # raise Exception('Output key names should be passed')
        else:
            if not isinstance(output_names, (list, tuple)):
                self.output_names = [output_names]
            else:
                self.output_names = output_names

        self.set_batch_size(batch_size)

        if self.stratified:
            self.make_stratified_indexs()

        if self.data_processor_config is not None:
            self.data_processor = DataProcessor(
                self.data_processor_config, log=False, default_processors=default_processors)
        else:
            self.data_processor = None

        if self.do_shuffle and self.order_column is None:
            self.shuffle()

        if in_memory:
            self.load_all_in_memory()

    def batch_index_to_samples(self, batch_index, stratified):
        # Generate indexes of the batch
        if not stratified:
            batch_index_ = np.arange(
                batch_index * self.batch_size, (batch_index + 1) * self.batch_size)
            index = np.take(self.index, batch_index_, mode='wrap')
        else:
            index_list = []
            for idx in self.instances_per_strata.index:
                index_ = np.arange(
                    index * self.instances_per_strata[idx], (index + 1) * self.instances_per_strata[idx])
                index_list.append(
                    np.take(self.strata_indexes[idx], index_, mode='wrap'))
            index = np.concatenate(index_list)
        return index

    def reset_generator(self):
        self.do_shuffle = False
        self.index = self.index_original
        self.set_training_phase(False)

    def shuffle(self):
        if self.stratified:
            for i in range(len(self.strata_indexes)):
                self.strata_indexes[i] = np.random.permutation(
                    self.strata_indexes[i])
        else:
            self.index = np.random.permutation(self.index)

    def set_batch_size(self, batch_size):
        self.batch_size = batch_size
        self.steps_per_epoch = int(np.ceil(len(self.index) / batch_size))

    def __len__(self):
        return self.steps_per_epoch

    def set_input_names(self, input_names):
        self.input_names = input_names

    def set_output_names(self, output_names):
        self.output_names = output_names

    def set_training_phase(self, training_phase):
        if self.data_processor is not None:
            self.data_processor.set_parameter_all_process(
                training_phase=training_phase)

    def load_all_in_memory(self):
        print('Loading {} data elements in memory'.format(len(self.data)))
        x, y = self.get_data_from_dataframe(self.data.iloc[:1])
        print('Total MBytes {}'.format(
            sum([a.size * a.itemsize for a in (x + y)]) * len(self.data) / 1024 / 1024))
        self.x_all, self.y_all = self.get_data_from_dataframe(self.data)
        self.in_memory = True

    def get_data_from_dataframe(self, data):
        if self.data_processor is not None:
            data = self.data_processor.process(data.to_dict('list'))
        x = [data[col_input] for col_input in self.input_names]
        y = [data[col_output] for col_output in self.output_names]
        return x, y

    def __getitem__(self, batch_index):
        sample_index = self.batch_index_to_samples(
            batch_index, self.stratified)
        if self.in_memory:
            ixs = np.where(np.isin(self.index, sample_index))[0]
            x_batch = [x_[ixs] for x_ in self.x_all]
            y_batch = [y_[ixs] for y_ in self.y_all]
        else:
            x_batch, y_batch = self.get_data_from_dataframe(
                self.data.reindex(sample_index))
        if len(x_batch) == 1:
            x_batch = x_batch[0]
        if len(y_batch) == 1:
            y_batch = y_batch[0]
        return x_batch, y_batch

    def on_epoch_end(self):
        if self.do_shuffle:
            self.shuffle()

    def get_features_size(self):
        n_features = {}
        for k, v in self.data.items():
            if k != 'index':
                if isinstance(v, pd.DataFrame):
                    if 'n_features' in v.columns:
                        n_features_ = v.n_features.unique()
                        if len(n_features_) > 1:
                            raise Exception('Mixed size features')
                        n_features[k] = n_features_[0]
                elif isinstance(v, pd.Series):
                    n_features[k] = np.vstack(v.values).shape[1]
        return n_features

    def make_stratified_indexs(self, target_col='y'):
        class_balance = self.data[self.stratified].value_counts()
        instances_per_strata = self.batch_size * \
            class_balance / (class_balance.sum())
        integer_part = np.floor(instances_per_strata)
        decimal_part = instances_per_strata - integer_part
        n_ceils = int(self.batch_size - np.sum(integer_part))
        sorted_decimals = np.argsort(decimal_part)
        for idx in instances_per_strata.index:
            if idx in sorted_decimals[-n_ceils:]:
                instances_per_strata[idx] = int(
                    np.ceil(instances_per_strata[idx]))
            else:
                instances_per_strata[idx] = int(
                    np.floor(instances_per_strata[idx]))
        self.instances_per_strata = instances_per_strata.astype(int)
        self.strata_indexes = {}
        for idx in self.instances_per_strata.index:
            self.strata_indexes[idx] = self.data[self.data[self.stratified]
                                                 == idx].index.values
        print('Ensuring each batch has: \n\n {}'.format(
            self.instances_per_strata))

        # if self.stratified_oversample:
        #    batches_per_class = self.batch_size / self.n_classes
        #    for i in range(len(batches_per_strata)):
        #        self.batches_per_strata[i] = int(batches_per_class)

        # self.stratified_weights = np.sum(self.batches_per_strata) / (self.batches_per_strata * self.n_classes)

        # gen_logger.warning('Class weights using stratified batches: {}'.format(str(self.stratified_weights)))
