import numpy as np
import pandas as pd
import librosa
import soundfile as sf
from deytah.core import Processor
from IPython import embed
from pathlib import Path
from collections.abc import Mapping


# TODO: tidy this function
def extend_to_fixed_size(x, fixed_samples, mode='trail'):
    if mode == 'trail':
        if x.size > fixed_samples:
            return x[:fixed_samples]
        elif x.size < fixed_samples:
            return np.r_[x, np.zeros(fixed_samples - x.size)]
        else:
            return x
    elif mode == 'lead':
        if x.size > fixed_samples:
            return x[-fixed_samples:]
        elif x.size < fixed_samples:
            return np.r_[np.zeros(fixed_samples - x.size), x]
        else:
            return x


class MappableReader(Processor):
    def setup(self, mapping):
        if not isinstance(mapping, Mapping):
            raise Exception('data must be of Mapping type, aka dict')
        self.mapping = mapping

    def operation(self, database=None, filename=None, start=None, duration=None, end=None, sr=None, fix_samples=None, fix_samples_mode='lead'):
        if start is None:
            start = [None] * len(filename)
        if duration is None:
            duration = [None] * len(filename)
        if sr is None:
            sr = [None] * len(filename)
        if end is None:
            end = [None] * len(filename)
        fix_samples = [fix_samples] * len(filename)

        def func(database, filename, start, duration, end, sr, fix_samples):
            if isinstance(filename, list) or isinstance(filename, np.ndarray):
                raise Exception('Not implemented')
            else:
                key = '{}/{}'.format(database, Path(filename).stem)
                audio = self.mapping[key]

                if start is None:
                    start = 0
                else:
                    start = int(sr * start)

                if duration is not None and end is None:
                    end = start + duration
                elif end is None:
                    end = len(audio)
                else:
                    end = int(sr * end)

                x = audio[start:end]

                if fix_samples is not None:
                    x = extend_to_fixed_size(
                        x, fix_samples, mode=fix_samples_mode)
            return x

        return list(map(func, database, filename, start, duration, end, sr, fix_samples))


class AudioReaderByFile(Processor):
    def operation(self, filename=None, start=None, duration=None, end=None, sr=None, fix_samples=None):
        if start is None:
            start = [None] * len(filename)
        if duration is None:
            duration = [None] * len(filename)
        if sr is None:
            sr = [None] * len(filename)
        if end is None:
            end = [None] * len(filename)
        fix_samples = [fix_samples] * len(filename)

        data = []
        groups = np.unique(filename)
        for g in groups:
            ix = np.where(np.array(filename) == g)[0]
            data.append((g, ix, np.array(start)[ix], np.array(duration)[ix], np.array(
                end)[ix], np.array(sr)[ix], np.array(fix_samples)[ix]))

        def groupfunc(filename, ix, starts, durations, ends, srs, fix_samples):

            if len(np.unique(srs)) > 1:
                raise Exception('Multiple sampling rate files')

            if isinstance(filename, list) or isinstance(filename, np.ndarray):

                # for i, filename_i in enumerate(filename):
                #     if isinstance(start, list) or isinstance(start, np.ndarray):
                #         offset_i = start[i]
                #     else:
                #         offset_i = offset

                #     if isinstance(duration, list) or isinstance(duration, np.ndarray):
                #         duration_i = duration[i]
                #     else:
                #         duration_i = duration

                #     if isinstance(sr, list) or isinstance(sr, np.ndarray):
                #         sr_i = sr[i]
                #     else:
                #         sr_i = sr

                #     x_i, sr_i = librosa.load(filename_i, sr=sr_i, duration=duration_i, offset=offset_i)

                #     if fix_samples is not None:
                #         x_i = self.extend_to_fixed_size(x_i, fix_samples)
                #     x.append(x_i)
                raise Exception('Not implemented')
            else:
                xs = []
                # print(start, end, duration, offset, sr)
                audio, sr = sf.read(filename)
                if sr != srs[0]:
                    raise Exception('Wrong samplerate file and data')

                for i, start, end, duration, fix_samples_ in zip(ix, starts, ends, durations, fix_samples):
                    if start is None:
                        start = 0
                    if duration is not None and end is None:
                        end = start + duration
                    x = audio[int(sr * start):int(sr * end)]

                    if fix_samples_ is not None:
                        x = extend_to_fixed_size(x, fix_samples_)
                    xs.append((i, x))
            return xs

        # transpose data, process with groupfun, concatenate and transpose again
        index, data = list(zip(*sum(list(map(groupfunc, *zip(*data))), [])))

        return pd.Series(data, index=index)


class AudioReader(Processor):
    def operation(self, filename=None, start=None, duration=None, end=None, sr=None, fix_samples=None):
        if start is None:
            start = [None] * len(filename)
        if duration is None:
            duration = [None] * len(filename)
        if sr is None:
            sr = [None] * len(filename)
        if end is None:
            end = [None] * len(filename)
        fix_samples = [fix_samples] * len(filename)

        def func(filename, start, duration, end, sr, fix_samples):
            if start is None:
                offset = 0
            else:
                offset = start
            if end is not None and end != -1:
                duration = end - start

            x = []

            if isinstance(filename, list) or isinstance(filename, np.ndarray):

                for i, filename_i in enumerate(filename):
                    if isinstance(start, list) or isinstance(start, np.ndarray):
                        offset_i = start[i]
                    else:
                        offset_i = offset

                    if isinstance(duration, list) or isinstance(duration, np.ndarray):
                        duration_i = duration[i]
                    else:
                        duration_i = duration

                    if isinstance(sr, list) or isinstance(sr, np.ndarray):
                        sr_i = sr[i]
                    else:
                        sr_i = sr

                    x_i, sr_i = librosa.load(
                        filename_i, sr=sr_i, duration=duration_i, offset=offset_i)

                    if fix_samples is not None:
                        x_i = extend_to_fixed_size(x_i, fix_samples)
                    x.append(x_i)
            else:
                # print(start, end, duration, offset, sr)
                x, sr = librosa.load(
                    filename, sr=sr, duration=duration, offset=offset)
                # audio, sr_ = sf.read(filename)
                # if sr != sr_:
                # raise Exception('Wrong samplerate file and data')

                # x = audio[int(sr * offset):int(sr * (offset + duration))]

                if fix_samples is not None:
                    x = extend_to_fixed_size(x, fix_samples)

            return x

        return list(map(func, filename, start, duration, end, sr, fix_samples))


class Abs(Processor):
    def operation(self, x):
        return pd.Series(map(np.abs, x))


class Phase(Processor):
    def operation(self, x):
        return pd.Series(map(np.angle, x))


class STFT(Processor):
    def operation(self, audio, sr, win_length, hop_length, fft_size=None, window='hann', center=True):
        win_length = int(sr * win_length / 1000)
        hop_length = int(sr * hop_length / 1000)
        if fft_size is None:
            fft_size = win_length

        def func(x):
            if isinstance(x, list):
                return [librosa.stft(x_i, win_length=win_length, hop_length=hop_length, n_fft=fft_size, window=window, center=center) for x_i in x]
            else:
                return librosa.stft(x, win_length=win_length, hop_length=hop_length, n_fft=fft_size, window=window, center=center)
        return pd.Series(map(func, audio))


class Polar_to_complex(Processor):
    def operation(self, magnitude=None, phase=None):
        def func(m, p):
            return m * np.exp(1j * p)
        return {'out_complex': pd.Series(map(func, magnitude, phase))}


class Stack(Processor):
    def operation(self, x):
        return np.stack(x, axis=0)


class Squeeze(Processor):
    def operation(self, x):
        return pd.Series(map(np.squeeze, x))


class Mixer(Processor):
    def operation(self, sources, gains):
        def func(sources, gains):
            sources = np.array(sources)
            gains = np.expand_dims(np.array(gains), axis=1)

            return np.sum(sources * gains, axis=0)
        return pd.Series(map(func, sources, gains))


class SelectFromList(Processor):
    def operation(self, list_name, list_ids, selection):
        def func(list_name, list_ids, selection):
            if not isinstance(selection, list):
                selection = [selection]
            idxs = [list_ids.index(selection_i) for selection_i in selection]
            out = [list_name[i]
                   for i, elem in enumerate(list_name) if i in idxs]
            return out
        return pd.Series(map(func, list_name, list_ids, selection))


class Resample(Processor):
    def operation(self, x, original_sr, target_sr):
        def func(x):
            if isinstance(x, list):
                y = [librosa.resample(x_i, original_sr, target_sr)
                     for x_i in x]
            else:
                y = librosa.resample(x, original_sr, target_sr)
            return y
        return pd.Series(map(func, x))


class Transpose(Processor):
    def operation(self, x, permutations):
        def func(x):
            return np.transpose(x, permutations)
        return pd.Series(map(func, x))


class Cut(Processor):
    def operation(self, x, sr=None, cut_points=[0, 2], unit='s'):
        '''
        Cuts the input signal between the specified range.
        cut_points        : start and end points to crop
        unit              : can be 's' for seconds or 'n' for samples
        sr                : if unit is 's', this is needed.
        '''
        if not sr:
            sr = [None] * len(x)

        ziped = zip(x, sr)

        def fn(x):
            signal, sr_i = x
            start, end = cut_points
            if unit == 's':
                if not sr:
                    raise Exception('Missing parameter sr')
                return signal[int(start * sr_i): int(end * sr_i)]
            elif unit == 'n':
                return signal[start:end]
            else:
                raise Exception('Unknown unit: {}'.format(unit))

        return pd.Series(map(fn, ziped))


class FilterColumns(Processor):
    def operation(self, x, columns):
        return x[columns]


class SelectField(Processor):
    def operation(self, x):
        return x


class LabelEncoder(Processor):
    def operation(self, x, n_labels=None, labels=None, no_detection_class=None):
        labels = sorted(labels)
        n_labels = len(labels)

        def func(x):
            hv = np.zeros(n_labels)
            if isinstance(x, str):
                if not (no_detection_class is not None and x == no_detection_class):
                    x = labels.index(x)
                    hv[x] = 1
            elif isinstance(x, dict):
                for k, v in x.items():
                    idx = labels.index(k)
                    hv[idx] = v
            return hv

        return list(map(func, x))


class Multiply(Processor):
    def operation(self, x, y):
        def func(x, y):
            return x * y
        return pd.Series(map(func, x, y))


class Normalize(Processor):
    def operation(self, x):
        def func(x):
            m = np.abs(x).max()
            if m > 0:
                x = x/m
            return x
        return pd.Series(map(func, x))
