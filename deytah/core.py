import networkx as nx
import copy
import pandas as pd
import inspect
from networkx.algorithms import bipartite
from networkx.algorithms.dag import topological_sort
from networkx.algorithms.components import connected_components
from ruamel.yaml import YAML

from .logger import get_logger
from .utils import add_dot, check_dot, get_modules
# from .mods import apply_mods

from pathlib import Path
from IPython import embed


class DataProcessor():
    graph = None
    processor_queue = None

    def __init__(self, config, log=True, default_processors=True):

        if log:
            self.log = get_logger('deytah')
        else:
            self.log = None

        if isinstance(config, str):
            self.config = self.read_config(config)
        else:
            self.config = config

        # if mods:
        #     if not isinstance(mods, list):
        #         mods = [mods]
        #     for mod in mods:
        #         self.config = apply_mods(self.config, mod)

        self.output = self.config.get('Output', [])
        self.processors = self.config.get('Processors', None)

        for processor_name, content in self.processors.items():
            if 'in' not in content:
                raise Exception(f'Missing input in process {processor_name}')
            if 'out' not in content:
                raise Exception(f'Missing output in process {processor_name}')

        # Lists to dict
        in_out_names = []
        for processor_name, content in self.processors.items():
            if 'processor' not in content:
                content['processor'] = processor_name
            for key in ['in', 'out']:
                temp_ = {}
                if isinstance(content[key], str):
                    if key == 'out':
                        temp_ = {0: add_dot(processor_name, content[key])}
                    else:
                        temp_ = {0: check_dot(content[key])}
                elif isinstance(content[key], list):
                    for i, v in enumerate(content[key]):
                        if key == 'out':
                            temp_[i] = add_dot(processor_name, v)
                        else:
                            temp_[i] = check_dot(v)
                elif isinstance(content[key], dict):
                    for k, v in content[key].items():
                        if key == 'out':
                            temp_[k] = add_dot(processor_name, v)
                        else:
                            temp_[k] = check_dot(v)
                content[key] = temp_
                for k, v in content[key].items():
                    # in_out_names.append(v.split('.')[1])
                    in_out_names.append(v)

        # Checking output names will exist
        if not isinstance(self.output, list):
            self.output = [self.output]

        for output in self.output:
            if output not in in_out_names:
                raise Exception(f'Missing field {output} in processors')

        # Following section loads modules with processors classes definitions
        module_paths = self.config.get('Module', [])
        if not isinstance(module_paths, list):
            module_paths = [module_paths]

        if default_processors:
            module_paths.append(
                str(Path(Path(__file__).parent, 'default_processors')))

        self.module_paths = module_paths
        self.modules = get_modules(self.module_paths)

        if self.log:
            self.log.info('Creating defs:')
        self.processor_defs = {}
        for processor_name, content in self.processors.items():
            processor_class_name = content['processor']
            for module in self.modules.values():
                if processor_class_name in dir(module):
                    if self.log:
                        self.log.info(f'Creating def {processor_class_name}')
                    self.processor_defs[processor_class_name] = getattr(
                        module, self.processors[processor_name]['processor'])

        if self.log:
            self.log.info('Creating instances:')
        self.processor_instance = {}
        for processor_name in self.processors.keys():
            processor_class_name = self.processors[processor_name]['processor']
            if self.log:
                self.log.info(f'Processor instances: {processor_name}')
            self.processor_instance[processor_name] = self.processor_defs[processor_class_name](
                processor_name, self.processors[processor_name], logger=self.log)

    def read_config(self, path):
        """
        Loads configuration files in YAML format into a python dictionary.
        # Arguments
            path: path to the configuration file.
        """

        with open(path, 'r') as config_file:
            if self.log:
                self.log.info('Reading yaml config: {}'.format(path))
            config = YAML(typ='safe').load(config_file)

        return config

    def make_graph(self):
        """
        Creates and returns a networkx graph with the processors nodes
        """
        graph_ = nx.DiGraph()
        for processor_name, content in self.processors.items():
            graph_.add_node(processor_name, bipartite=0)
            for k, v in content['in'].items():
                graph_.add_node(v, bipartite=1)
                graph_.add_edge(v, processor_name)
            for k, v in content['out'].items():
                graph_.add_node(v, bipartite=1)
                graph_.add_edge(processor_name, v)

        graph_ = bipartite.projected_graph(graph_, self.processors.keys())

        edges = list(graph_.edges())

        components_subgraphs = [graph_.subgraph(
            c) for c in connected_components(graph_.to_undirected())]

        # if not nx.is_connected(graph_.to_undirected()):
        #    raise Exception('Processors graph is not connected')

        return components_subgraphs, edges

    def get_processor_queue(self):
        """
        Returns a list of processor in dependencies order
        """
        if self.graph is None:
            self.graph, edges = self.make_graph()

        sortings = []
        for subgraph in self.graph:
            sortings.append(list(topological_sort(subgraph)))

        return sortings

    def set_parameter_all_process(self, **kwargs):
        for processor_name, processor_settings in self.processors.items():
            processor_class_name = self.processors[processor_name]['processor']
            operation = self.processor_defs[processor_class_name].operation
            args = inspect.getargspec(operation)
            for k, v in kwargs.items():
                if k in args.args:
                    processor_settings[k] = v

    def process(self, df_):
        # TODO: check if copy is needed
        df = df_.copy()
        if self.processor_queue is None:
            self.processor_queue = self.get_processor_queue()

        output = {}
        for queue in self.processor_queue:
            if any([o.split('.')[0] in queue for o in self.output]):
                for processor_name in queue:
                    pi = self.processor_instance[processor_name]
                    out = pi.process(df)

                    for k, v in self.processors[processor_name]['out'].items():
                        _v = v.split('.')[1]
                        if v in self.output:
                            if pi.out_as == 'array':
                                output[v] = out
                            elif pi.out_as == 'dict' or pi.out_as == 'dataframe':
                                output[v] = out[_v]
                    # for k, v in self.processors[processor_name]['in'].items():
                    #     print(k,v)
                    #     _v = v.split('.')[1]
                    #     if v in self.output:
                    #         if processor_ins.out_as == 'array':
                    #             output[v] = out
                    #         elif processor_ins.out_as == 'dict' or processor_ins.out_as == 'dataframe':
                    #             output[v] = out[_v]
        output = {k.split('.')[1]: v for k, v in output.items()}
        return output

    def __getstate__(self):
        state = self.__dict__.copy()
        del state['modules']

        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        self.modules = get_modules(self.module_paths)


class Processor():
    def __init__(self, name, config, logger):
        self.logger = logger
        self.name = name
        self.not_parameters = ['in', 'out', 'processor', 'out_as']
        self.set_config(config)

    def set_config(self, config):
        self.config = config
        self.out_field = {k: v.split('.')[1] for k, v in config['out'].items()}
        self.in_field = {k: v.split('.')[1] for k, v in config['in'].items()}
        self.out_as = config.get('out_as', 'dataframe')

        # las 4 lineas siguientes entran en una sola, desafío
        self.parameters = copy.deepcopy(config)

        for attribute in self.not_parameters:
            if attribute in self.parameters:
                self.parameters.pop(attribute)

    def process(self, df):
        if self.logger:
            self.logger.info(f'Processing {self.name}')
        args = {}
        kwargs = {}

        for k, v in self.in_field.items():
            if isinstance(k, str):
                if v in df.keys():
                    kwargs[k] = df[v]
                else:
                    raise Exception(f'Missing field {v} in dataframe or dict')
            elif isinstance(k, int):
                if v in df.keys():
                    args[k] = df[v]
                else:
                    raise Exception(f'Missing field {v} in dataframe or dict')
        # dict {0:x,1:y} to list [x,y]
        args = [v for k, v in sorted(args.items())]
        kwargs.update(self.parameters)

        if self.operation is not None:
            try:
                output = self.operation(*args, **kwargs)
            except Exception as e:
                raise Exception(
                    f'Could not process {self.name} with error:\n\n' + str(e))
        else:
            raise Exception(
                'Missing operation method, the child class should define that method')

        if self.out_as == 'dataframe' or self.out_as == 'dict':
            if not (isinstance(output, tuple) or isinstance(output, dict)):
                output = {0: output}
            elif isinstance(output, tuple):
                output = {i: v for i, v in enumerate(output)}
            assert len(output) == len(self.out_field)
            for k, v in output.items():
                df[self.out_field[k]] = v
            return df
        elif self.out_as == 'array':
            if isinstance(output, (pd.Series, pd.DataFrame)):
                output = output.to_numpy()
            return output
