#!/usr/local/bin/python3.6

import numpy as np
import pandas as pd
import librosa
import shutil
from IPython import embed
from deytah.core import DataProcessor
from deytah.batch_generator import BatchGenerator
from pathlib import Path

N = 100
raw = []
for i in range(N):
    raw.append(np.random.uniform(low=-1, high=1, size=(44100,)))

data = []
sr = 16000

Path('dummy').mkdir(parents=True, exist_ok=True)

for i in range(N):
    filename = f'dummy/{i}.wav'
    data.append({'filename': filename, 'duration': 1.0, 'sr': sr,'y':i})
    librosa.output.write_wav(filename, raw[i], sr=sr)

data = pd.DataFrame(data)

bg = BatchGenerator(data=data, input_names='x', output_names='y', data_processor_config='test_batch_gen2.yaml')
print(bg[0])

shutil.rmtree('dummy')
