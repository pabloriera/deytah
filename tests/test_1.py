#!/usr/local/bin/python3.6

import numpy as np
import pandas as pd
import librosa
import shutil
from IPython import embed
from deytah.core import DataProcessor
from deytah.default_processors import *
from pathlib import Path

N = 1000
raw = []
for i in range(N):
    raw.append(np.random.uniform(low=-1, high=1, size=(44100,)))

data = []
sr = 16000

Path('dummy').mkdir(parents=True, exist_ok=True)

for i in range(N):
    filename = f'dummy/{i}.wav'
    data.append({'filename': filename, 'duration': 1.0, 'sr': sr})
    librosa.output.write_wav(filename, raw[i], sr=sr)

test_df = pd.DataFrame(data)


data_proc = DataProcessor('test_1.yaml')

proc_ar = AudioReader('AudioReader', data_proc.config['Processors']['AudioReader'])
proc_abs = Abs('Abs', data_proc.config['Processors']['Abs'])
proc_stft = STFT('STFT', data_proc.config['Processors']['STFT'])
proc_phase = Phase('Phase', data_proc.config['Processors']['Phase'])
proc_polar = Polar_to_complex('Polar_to_complex', data_proc.config['Processors']['Polar_to_complex'])
proc_stack_spec = Stack('Stack_spectrogram', data_proc.config['Processors']['Stack_spectrogram'])
proc_stack_raw = Stack('Stack_raw_audio', data_proc.config['Processors']['Stack_raw_audio'])

new_df = proc_ar.process(test_df)
new_df = proc_stft.process(new_df)
new_df = proc_phase.process(new_df)
new_df = proc_abs.process(new_df)
new_df = proc_polar.process(new_df)
stacked_spec = proc_stack_spec.process(new_df)
stacked_raw = proc_stack_raw.process(new_df)


dp = DataProcessor('test_1.yaml')
output = dp.process(test_df)


shutil.rmtree('dummy')
