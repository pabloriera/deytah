#!/usr/local/bin/python3.6

import numpy as np
import pandas as pd
import librosa
import shutil
from IPython import embed
from deytah.core import DataProcessor
from pathlib import Path

N = 100
raw = []
for i in range(N):
    raw.append(np.random.uniform(low=-1, high=1, size=(44100,)))

data = []
sr = 16000

Path('dummy').mkdir(parents=True, exist_ok=True)

for i in range(N):
    filename = f'dummy/{i}.wav'
    data.append({'filename': filename, 'duration': 1.0, 'sr': sr})
    librosa.output.write_wav(filename, raw[i], sr=sr)

data = pd.DataFrame(data)
data = data.to_dict('list')

dp = DataProcessor('test_2.yaml')
output = dp.process(data)

print(data.keys())
print(output['spectrogram'].shape)

shutil.rmtree('dummy')
