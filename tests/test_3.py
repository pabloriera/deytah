#!/usr/local/bin/python3.6

import numpy as np
import pandas as pd
import librosa
import shutil
from IPython import embed
from deytah.core import DataProcessor
from pathlib import Path

N = 100
raw = []
for i in range(N):
    raw.append(np.random.uniform(low=-1, high=1, size=(44100,)))

data = []
sr = 16000

Path('dummy').mkdir(parents=True, exist_ok=True)

for i in range(N):
    filename = f'dummy/{i}.wav'
    data.append({'filename': filename, 'duration': 100.0, 'sr': sr})
    librosa.output.write_wav(filename, raw[i], sr=sr)

test_df = pd.DataFrame(data)

import timeit

cmd = """
dp = DataProcessor('test_2.yaml', log=False)
output = dp.process(test_df)
"""
print(timeit.timeit(cmd, globals=globals(), number=10))

cmd = """
dp = DataProcessor('test_3.yaml', log=False)
output = dp.process(test_df)
"""
print(timeit.timeit(cmd, globals=globals(), number=10))

shutil.rmtree('dummy')
